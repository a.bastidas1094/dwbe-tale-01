const express = require('express');
const router = express.Router();

const { agregarUsuario, obtenerUsuarios } = require('../models/usuario.model');

/**
 * @swagger
 * /usuarios:
 *  get:
 *      summary: Obtener todos los usuarios del sistema
 *      tags: [Usuarios]
 *      responses:
 *          200:
 *              description: Lista de usuarios del sistema
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/usuario'
 */
 router.get('/', (req, res) => {
    // console.log(req.auth.user);
    res.json(obtenerUsuarios());
})

/**
 * @swagger
 * /usuarios:
 *  post:
 *      summary: Crea un usuario en el sistema
 *      tags: [Usuarios]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/usuario'
 *      responses:
 *          201:
 *              description: Usuario creado
 *          401:
 *              description: usuario y contrasena incorrectos
 */
 router.post('/', (req, res) => {
    res.sendStatus(201);
})

router.put('/:email', (req, res) => {
    console.log(req.body);
    res.json(usuario);
});

router.delete('/:email', (req, res) => {
    res.json('Usuario eliminado');
});

/**
 * @swagger
 * tags:
 *  name: Usuarios
 *  description: Seccion de usuarios
 * 
 * components: 
 *  schemas:
 *      usuario:
 *          type: object
 *          required:
 *              -email
 *              -contrasena
 *          properties:
 *              email:
 *                  type: string
 *                  description: Email del usuario
 *              contrasena:
 *                  type: string
 *                  description: Contrasena del usuario
 *          example:    
 *              email: usuario@gmail.com
 *              contrasena: 1234abc
 */

module.exports = router;

const usuarios = [
    {
        username: "Mauricio",
        password: "12345",
        isAdmin: true
    },
    {

        username: "admin",
        password: "12345",
        isAdmin: true
    }
];

const obtenerUsuarios = () => {
    return usuarios;
}

const agregarUsuario = (usuarioNuevo) => {
    usuarios.push(usuarioNuevo);
}

module.exports = {obtenerUsuarios, agregarUsuario};
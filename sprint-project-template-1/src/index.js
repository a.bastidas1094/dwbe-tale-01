const express = require('express');
const app = express();
const basicAuth = require('express-basic-auth')
const autenticacion = require('./middlewares/autenticacion.middleware');

//rutas de registro y login antes, (aca) para que se ejecuten primero

app.use(express.json());

app.get('/login', (req, res) => {
    res.json('sin autenticacion');
});

app.post ('/registrarUsuario', (req, res) => {
    res.json('sin autenticacion');
});

app.use(basicAuth({ authorizer: autenticacion }));

const usuarioRoutes = require('./routes/usuario.routes');

app.use('/usuarios', usuarioRoutes);

app.listen(3000, () => { console.log('Escuchando en el puerto 3000') });

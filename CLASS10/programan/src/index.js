const coolImages = require("cool-images");

const urlImagenes = coolImages.many(720, 1080, 10, false, false);
// console.log(urlImagenes);

urlImagenes.forEach((urlImagen, i) => {
    console.log("Url " + (i + 1) + ": " + urlImagen);
});

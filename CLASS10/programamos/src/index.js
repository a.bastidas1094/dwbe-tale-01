var moment = require('moment'); 
console.log('Horario Local' + moment().format()); // horario local si ("dddd, MMMM Do YYYY, h:mm:ss a")
console.log('Horario UTC: ' + moment.utc().format());

const diferencia = moment().hours() - moment.utc().hour();
console.log(`${moment().utc().hour()} - ${moment().hour()} = ${diferencia - 24}`);
console.log('UTC offset: ' + (moment().utcOffset() / 60));

const fechaInicial = moment('2060-01-01');

    if(fechaInicial.isBefore('2050-01-01')) {
    console.log('La fecha inicial es anterior al año 2050.');
}   else {
    console.log('La fecha inicial es posterior al año 2050.');
}

console.log('Otro cambio');

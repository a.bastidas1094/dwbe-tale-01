class Persona {
    constructor(nombre, apellido, edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    getFullName() {
        return this.nombre + " " + this.apellido;
    }

    es_mayor() {
        return this.edad >= 18;
    }
}

let personas = [];

for (let index = 0; index < 5; index++) {
    let persona1 = new Persona("Juan", "Puello", index);
    personas.push(persona1);
}

console.log(personas);
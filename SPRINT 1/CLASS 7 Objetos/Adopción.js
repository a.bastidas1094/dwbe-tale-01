class Perro {
    constructor(nombre, raza, edad, color) {
        this.nombre = nombre;
        this.raza = raza;
        this.edad = edad;
        this.color = color;
        this.estadoAdopcion = "En adopción";
    }

    obtenerEstadoAdopcion() {
        return this.estadoAdopcion;
    }

    establecerEstadoAdopcion(estado) {
        const estados = ['En adopción', 'Proceso de adopción', 'Adoptado'];
        this.estadoAdopcion = estados[estado - 1];
    }

}

function imprimirListas(lista, mensaje) {

    for (const perro of lista) {
        mensaje += `\n${perro.nombre}`;
    }
    console.log(mensaje);
}

let validacion = true;
const perros = [];

while (validacion) {
    nombre = prompt("Nombre del perro: ");
    raza = prompt("Raza del perro: ");
    edad = parseInt(prompt("Edad del perro: "));
    color = prompt("Color del perro: ");
    estadoAdopcion = parseInt(prompt("Estado de adopcion (1: En adopción,2: Proceso de adopción,3: Adoptado): "));

    let nuevoPerro = new Perro(nombre, raza, edad, color);
    nuevoPerro.establecerEstadoAdopcion(estadoAdopcion);
    perros.push(nuevoPerro);

    validacion = window.confirm("Quiere Continuar?");
}

imprimirListas(perros, "Todos los perros:");
imprimirListas(perros.filter(perro => perro.obtenerEstadoAdopcion() === "En adopción"), "Perros en adopción");
imprimirListas(perros.filter(perro => perro.obtenerEstadoAdopcion() === "Proceso de adopción"), "Perros en proceso de adopción");
imprimirListas(perros.filter(perro => perro.obtenerEstadoAdopcion() === "Adoptado"), "Perros adoptados");

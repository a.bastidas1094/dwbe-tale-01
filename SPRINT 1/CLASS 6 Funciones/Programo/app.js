// function funcionPrueba(nombre, apellido) {
//     console.log('Hola desde una funcion ' + nombre + ' ' + apellido);;
// }

// funcionPrueba("Mauricio", "Sierra");
// funcionPrueba("Lionel", "Messi");
// funcionPrueba("Ronaldo", "Cristiano");

// CONST y LET vs VAR (no usar var)
// var texto1 = "Texto ejemplo";
// var texto1 = "Texto ejemplo!!!!";

// let texto2 = "Hola LET JS";
// texto2 = "Hola LET JS!!!!!!";

// const texto3 = "Hola desde constante";

// if(true) {
//     let textoIf = 'hola desde dentro del if';
//     console.log(textoIf);
//     console.log(texto2);
// }

// function dentroDeFuncion() {
//     var texto4 = "Hola texto 4";
//     return texto4;
// }

// console.log(dentroDeFuncion());

// Funciones flecha:
function saludar() {
    console.log('Hola');
}

let saludar2 = function (e) {
    console.log('Hola2');
}

let saludar3 = () => {
    console.log('Hola3');
}

// saludar();
// saludar2();
// saludar3();

document.getElementById('btn').addEventListener('click', saludar3);

function suma(n1, n2) {
    console.log(n1 + n2);
}

let suma2 = function (n1, n2) {
    console.log(n1 + n2);
}

let suma3 = (n1, n2) => {
    console.log(n1 + n2);
}

let suma4 = (n1, n2) => console.log(n1 + n2);

let elDoble = n1 => n1 * 2;

suma(1, 2);
suma2(1, 2);
suma3(1, 2);
suma4(1, 2);

console.log(elDoble(5));
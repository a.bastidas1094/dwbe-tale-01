
const hobbies = ['Leer', 'Jugar futbol', 'Cantar', 'Correr', 'Ver peliculas'];
const fs = require('fs');

hobbies.forEach((hobbie, indice) => {
    const mensaje = `${indice + 1}. ${hobbie}\n`;
    console.log(mensaje);
    fs.appendFileSync('listaHobbies.txt', mensaje, function (err) {
        if (err) console.log(err);
        else console.log('Saved!');
    });
});

const x = require('fs');

function sumar(numero1, numero2){

    escribir("SUMA: "+numero1 +" + "+numero2+" = "+(numero1 + numero2));
    
    return numero1 + numero2;
}

function restar(numero1, numero2){
    escribir("RESTA: "+numero1 +" - "+numero2+" = "+(numero1 - numero2));

    return numero1 - numero2;
}

function multiplicar(numero1, numero2){
    escribir("MULTIPLICACIÓN: "+numero1 +" * "+numero2+" = "+(numero1 * numero2));
    return numero1*numero2;
}

function dividir(numero1, numero2){
    escribir("DIVISIÓN: "+numero1 +" / "+numero2+" = "+(numero1 / numero2));
    return numero1/numero2;
}

function escribir(texto){
    x.appendFileSync('./registro.txt',"\n"+texto, (err)=>{
        if(err) console.log(err);
        
    });
}

exports.module = {
    dividir, multiplicar, restar, sumar
}

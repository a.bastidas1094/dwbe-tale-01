
const calculator = require('./calculator.js');

const suma = {
    'Operacion': 'suma',
    'Valor1': 2,
    'Valor2': 3
}

const resta = {
    'Operacion': 'resta',
    'Valor1': 5,
    'Valor2': 6
}

const multiplicacion = {
    'Operacion': 'multiplicacion',
    'Valor1': 6,
    'Valor2': 5
}

const division = {
    'Operacion': 'division',
    'Valor1': 5,
    'Valor2': 2
}

const operaciones = [suma, resta, multiplicacion, division];

operaciones.forEach(operacion=>{
    const valor1 = operacion.Valor1;
    const valor2 = operacion.Valor2;
    switch(operacion.Operacion){
        case 'suma': {
            console.log(operacion.Operacion+": "+calculator.module.sumar(valor1, valor2));
        }break;
        case 'resta': {
            console.log(operacion.Operacion+": "+calculator.module.restar(valor1, valor2));
        }break;
        case 'multiplicacion': {
            console.log(operacion.Operacion+": "+calculator.module.multiplicar(valor1, valor2));
        }break;
        case 'division': {
            console.log(operacion.Operacion+": "+calculator.module.dividir(valor1, valor2));
        }break;
    }

})



require('dotenv').config();
const express = require('express');
const app = express();

const PORT = process.env.PORT || 3000;

app.use(express.static('./src/public'));
app.use(express.json());

const lista_usuarios = ["user1", "user2", "user3"];

// Para obtener usuarios
app.get('/usuarios', (req, res) => {
    res.json(lista_usuarios);
});

// Para crear usuarios
app.post('/usuarios', (req, res) => {
    const { nombre } = req.body;
    lista_usuarios.push(nombre);
    res.json('Usuario creado exitosamente');
});

// Para actualizar usuarios
app.put('/usuarios', (req, res) => {
    const { index } = req.query;
    const { nombre } = req.body;
    lista_usuarios[index] = nombre;
    res.json('Usuario actualizado');
});

// Para borrar usuarios
app.delete('/usuarios', (req, res) => {
    const { index } = req.query;
    lista_usuarios.splice(index, 1);
    res.json('Usuario eliminado');
});

app.listen(PORT, () => {
    console.log('Escuchando desde el puerto ' + PORT);
});


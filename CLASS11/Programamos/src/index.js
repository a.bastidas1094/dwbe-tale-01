const express = require('express');
const app = express();
const { obtenerTelefonos, obtenerMitadTelefonos } = require('./models/Telefonos');

app.get('/telefonos', (req, res) => {
    res.json(obtenerTelefonos());
});

app.get('/mitadtelefonos', (req, res) => {
    res.json(obtenerMitadTelefonos());
});

app.listen(3000, () => {
    console.log('Escuchando en el puerto 3000');
});


const telefonos = [
    {
        "marca" : "Samsung 1",
        "gama" : "Alta",
        "modelo": "S11",
        "pantalla": "19:9",
        "psistema_operativo" : "Andrioid",
        "precio": 1200
    },

    {
        "marca" : "Samsung 2",
        "gama" : "Alta",
        "modelo": "S11",
        "pantalla": "19:9",
        "psistema_operativo" : "Andrioid",
        "precio": 900
    },

    {
        "marca" : "Samsung 3",
        "gama" : "Alta",
        "modelo" : "S11",
        "pantalla": "19:9",
        "psistema_operativo" : "Andrioid",
        "precio": 2000
    }

];

const obtenerTelefonos = ()=>{
    return telefonos;
}

const obtenerMitanTelefonos = ()=>{
    
    const mitad = Math.round(telefonos.length/2);
    let mitadTelefonos = [];
    for(let i=0;i<mitad;i++){
        mitadTelefonos.push(telefonos[i]);
    }
    
    return mitadTelefonos;
}

const obtenerTelefonoPrecioMasBajo = ()=>{

    let telefonoMasBajo = telefonos[0];
    let precioMasBajo = telefonoMasBajo.precio;
    
    for(let i=1;i<telefonos.length;i++){

        if(precioMasBajo >telefonos[i].precio){
            telefonoMasBajo = telefonos[i];
            precioMasBajo = telefonoMasBajo.precio;   
        }

    }

    return telefonoMasBajo;

};

const obtenerTelefonoPrecioMasAlto= ()=>{

    let telefonoMasAlto = telefonos[0];
    let precioMasAlto = telefonoMasAlto.precio;
    
    for(let i=1;i<telefonos.length;i++){

        if(precioMasAlto < telefonos[i].precio){
            telefonoMasAlto = telefonos[i];
            precioMasBajo = telefonoMasAlto.precio;   
        }

    }

    return telefonoMasAlto;

};




module.exports = {obtenerMitanTelefonos, obtenerTelefonos, obtenerTelefonoPrecioMasAlto, obtenerTelefonoPrecioMasBajo};

let express = require('express');
let app = express();
let lista = ['camila','mariana','alejandra','bianca','clara','sandra'];

app.get('/', function (req, res) {
    res.send('Hola mundo');
})

app.listen(3000, function () {
    console.log('Escuchando el puerto 3000!');
});

app.get('/saludo', function (req, res) {
    res.send('Hola mundo function');
});

app.get('/compa', function (req, res) {
    let msg='';
    lista.forEach((compa,i) => {
        msg+= `${i + 1}) ${compa}`;
        msg+="\n";
    });
    res.send(msg);
})